$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("com.test/Feature1.feature");
formatter.feature({
  "line": 2,
  "name": "Navigate to Webiste and add a product to basket",
  "description": "",
  "id": "navigate-to-webiste-and-add-a-product-to-basket",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 4,
  "name": "I need to navigate to a website and add product to basket",
  "description": "",
  "id": "navigate-to-webiste-and-add-a-product-to-basket;i-need-to-navigate-to-a-website-and-add-product-to-basket",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 3,
      "name": "@Run2"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "I open a browser chrome",
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "I navigate to a Website",
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "I add a product to basket",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "I should go to basket page",
  "keyword": "Then "
});
formatter.match({
  "location": "Feature1Test.I_open_a_browser_chrome()"
});
formatter.result({
  "duration": 1135218933,
  "status": "passed"
});
formatter.match({
  "location": "Feature1Test.i_navigate_to_a_Website()"
});
formatter.result({
  "duration": 94467,
  "status": "passed"
});
formatter.match({
  "location": "Feature1Test.I_add_a_product_to_basket()"
});
formatter.result({
  "duration": 74488,
  "status": "passed"
});
formatter.match({
  "location": "Feature1Test.I_should_go_to_basket_page()"
});
formatter.result({
  "duration": 37939,
  "status": "passed"
});
});