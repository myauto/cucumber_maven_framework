package com.test.hooks;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.test.configuration.Configuration;
import com.test.configuration.ConfigurationProvider;
import com.test.configuration.PageFactory;
import com.test.configuration.WebDriverProvider;
import com.test.exceptions.StopTestException;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.CommandExecutor;
import org.openqa.selenium.remote.HttpCommandExecutor;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;

/**
 * Created by Karthik on 22/03/2016.
 */
public class BrowserHooks {

    @Inject
    public WebDriver driver;

    @Before
    public void setup(Scenario scenario) throws StopTestException, MalformedURLException{
        String browser = Configuration.getConfiguration().getSafeBrowser();
    }

    @After
    public void close() throws StopTestException{
        PageFactory.tearDown();
    }

}
