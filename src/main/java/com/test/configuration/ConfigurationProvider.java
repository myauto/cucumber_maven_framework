package com.test.configuration;

import com.test.exceptions.StopTestException;
import com.google.inject.Provider;

public class ConfigurationProvider implements Provider<Configuration> {
    @Override
    public Configuration get() {
        try {
            return Configuration.getConfiguration();
        } catch (StopTestException e) {
            e.printStackTrace();
        }

        return null;
    }
}
