package com.test.configuration;

import com.test.exceptions.StopTestException;
import com.test.helper.*;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.io.TemporaryFilesystem;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class PageFactory {
    private static WebDriver driver = null;

    private static WebDriver remoteWebDriver = null;

    private static final String SAFARI_BROWSER = "SAFARI";

    public static Object getPage(String pageClassName)
            throws StopTestException {

        String applicationMode = Configuration.getConfiguration()
                .getApplicatonMode();

        if ("MOBILE".equals(applicationMode)) {
            pageClassName = pageClassName + "Mobile";
        }
        // removed until all pages have been split, can then be brought back
        // with the if statement below being removed
        // else {
        // pageClassName = pageClassName + "Web";
        // }
        Object page = null;
        try {
            Class<?> pageClass = Class.forName(pageClassName);
            // if statement below is to allow non split pages to work until all
            // pages have been split
            if (pageClass.isInterface()) {
                pageClassName = pageClassName + "Web";
                pageClass = Class.forName(pageClassName);
            }
            LoggingHelper.printDebug("Loading page: " + pageClassName);
            Constructor<?> constructor = pageClass
                    .getConstructor(WebDriver.class);
            page = constructor.newInstance(getDriver());
        } catch (Exception ex) {
            throw new StopTestException(
                    "could not get page : " + pageClassName);
        }
        return page;
    }

    /**
     * Method tp switch to an iframe on a page - elements inside an iFrame cannot be accessed unless
     * the driver switches to inside the iFrame
     *
     * @param element
     */
    public static void switchToIframe(WebElement element) {
        getDriver().switchTo().frame(element);
    }


    /**
     * Create WebDriver instance if it doesn't exist yet
     */
    public static WebDriver getDriver() {

        try {
            if (Configuration.getConfiguration().getGridRun()
                    .equalsIgnoreCase("true")) {
                remoteWebDriver.getTitle();
                return remoteWebDriver;
            } else {
                driver.getTitle();
                return driver;
            }

        } catch (Exception e) {
            setUp();
            return driver;
        }

    }

    /**
     * Create Firefox WebDriver instance after closing other driver instances
     *
     * @throws StopTestException
     */
    public static WebDriver getFireFoxDriver() throws StopTestException {

        // Check to see if firefox is the current driver quit if not
        if (driver != null) {
            if (!(driver instanceof FirefoxDriver)) {
                try {
                    driver.quit();
                } catch (Exception e) {
                    // Ignore

                }
            }
        }

        // See if firefox driver is running if not start a firefox driver
        try {
            driver.getTitle();
            return driver;
        } catch (Exception e) {
            // firebug xpi can be found in etc/windows
            FirefoxProfile profile = new FirefoxProfile();

            if (Configuration.getConfiguration().getDebugProperty()
                    .equalsIgnoreCase("TRUE")) {
                final String firebugPath = "C:\\firebugxpi\\firebug.xpi";
                try {
                    profile.addExtension(new File(firebugPath));
                    profile.setPreference("extensions.firebug.currentVersion",
                            "1.7.3");
                } catch (Exception e1) {
                    System.out.println("unable to firebug extention");
                }
            }

            profile.setPreference("network.proxy.type", 5);
            profile.setPreference("network.http.phishy-userpass-length", 255);
            driver = new FirefoxDriver(profile);
            driver.manage().deleteAllCookies();
            return driver;
        }

    }

    /**
     * Get currently used driver instance and don't create new one if it doesn't
     * exists
     *
     * @throws StopTestException
     */
    public static WebDriver getCurrentDriver() throws StopTestException {
        try {
            if (Configuration.getConfiguration().getGridRun()
                    .equalsIgnoreCase("true")) {
                return remoteWebDriver;
            } else {
                return driver;
            }
        } catch (Exception e) {
            throw new StopTestException("Unable to getCurrentDriver()");
        }

    }

    /**
     * Kills WebDriver instance if it exists
     */
    public static void tearDown() {
        String debug = "false";
        try {
            debug = Configuration.getConfiguration().getDebugProperty();
        } catch (Exception a) {
            // do nothing.
            // debug set to false above.

        }
        if (driver != null) {

            try {
                TemporaryFilesystem.getDefaultTmpFS().deleteTemporaryFiles();
            } catch (Exception e1) {
                // Ignore
            }
            try {
                driver.manage().deleteAllCookies();

            } catch (Exception e) {
                // Ignore
            }

            try {
                driver.quit();
            } catch (Exception e) {
                System.out
                        .println(
                                "In tearDown() - Exception thrown when attempting driver.quit() "
                                        + e.getMessage());
            } finally {
                driver = null;
            }
        }

        // RemoteWebDriver instances (GRID Runs!)
        if (remoteWebDriver != null) {
            try {
                TemporaryFilesystem.getDefaultTmpFS().deleteTemporaryFiles();
            } catch (Exception e1) {
                if (debug.equalsIgnoreCase("true")) {
                    System.out
                            .println(
                                    "Exception thrown in tearDown(). Unable to delete temporary files. "
                                            + e1.getMessage());
                }

            }
            // Cookie Handling.
            try {
                if (debug.equalsIgnoreCase("true")) {
                    Set<Cookie> cookies = remoteWebDriver.manage().getCookies();
                    Iterator<Cookie> cookiesIter = cookies.iterator();
                    System.out
                            .println(
                                    "Before remoteWebDriver.manage().deleteAllCookies()");
                    while (cookiesIter.hasNext()) {
                        System.out.println(cookiesIter.next());
                    }
                }
                remoteWebDriver.manage().deleteAllCookies();
                if (debug.equalsIgnoreCase("true")) {

                    Set<Cookie> cookies = remoteWebDriver.manage().getCookies();
                    Iterator<Cookie> cookiesIter = cookies.iterator();
                    System.out
                            .println(
                                    "After remoteWebDriver.manage().deleteAllCookies()");
                    while (cookiesIter.hasNext()) {
                        System.out.println(cookiesIter.next());
                    }
                }
            } catch (Exception e) {
                if (debug.equalsIgnoreCase("true")) {
                    System.out
                            .println(
                                    "Exception thrown in tearDown(). Unable to delete cookies. "
                                            + e.getMessage());
                }
            }

            // Screenshot and page source handling.
            try {
                String stopTest = "false";
                if (System.getProperty("autotest.stop_test") != null) {
                    stopTest = System.getProperty("autotest.stop_test");
                }
                if (debug.equalsIgnoreCase("true")
                        && stopTest.equalsIgnoreCase("true")) {

                    ReportingHelper.takeGridScreenShot();
                    ReportingHelper.getPageSourceCode();

                }

            } catch (Exception e) {
                System.out
                        .println(
                                "Unable to perform grid screenshot/page source handling"
                                        + e.getMessage());
            }

            // Quit RemoteWebDriver.
            try {
                remoteWebDriver.quit();
            } catch (Exception e) {
                System.out
                        .println(
                                "In tearDown() - Exception thrown when attempting remoteWebDriver.quit() "
                                        + e.getMessage());
            } finally {
                remoteWebDriver = null;
            }
        }
    }

    /**
     * Creates WebDriver instance and opens ie browser
     */
    public static void setUp() {
        // changing default browser to Firefox
        driver = new FirefoxDriver();
    }

    public static void deleteAllCookies() {
        if (driver != null) {
            driver.manage().deleteAllCookies();
        }
    }

    public static void deleteNamedCookie(String cookieName) {
        if (driver != null) {
            driver.manage().deleteCookieNamed(cookieName);
        }
    }

    public static void closeBrowser() {
        if (driver != null) {
            try {
                driver.quit();
            } catch (Exception e) {
                // Ignore

            }
            //WaitHelper.getInstance().sleepQuietly(1000);
        }

    }

    public static void refocusDriver() {
        if (driver != null) {
            try {
                driver.switchTo().defaultContent();
            } catch (Exception e) {
                // Ignore

            }
        }
        if (remoteWebDriver != null) {
            try {
                remoteWebDriver.switchTo().defaultContent();
            } catch (Exception e) {
                // Ignore

            }
        }

    }

    public static void addCookie(String key, String value)
            throws StopTestException {
        WebDriver driver = PageFactory.getCurrentDriver();
        // System.out.println("Cookie details: "
        // + driver.manage().getCookies().toString());
        Cookie newCookie = new Cookie(key, value);
        driver.manage().addCookie(newCookie);
        System.out.println(key + "Cookie Added: "
                + driver.manage().getCookieNamed(key).toString());
        // System.out.println("Cookie details: "
        // + driver.manage().getCookies().toString());
    }

    /**
     * Adding Cookie wherein we specify the domain as well
     * @param key
     * @param value
     * @param domain
     * @throws StopTestException
     */
    public static void addCookie(String key, String value,String domain)
            throws StopTestException {
        WebDriver driver = PageFactory.getCurrentDriver();
        Cookie newCookie = new Cookie(key, value, domain,null, null);
        driver.manage().addCookie(newCookie);
    }
    /**
     * This method returns the value of the name cookie
     *
     * @param cookieName
     * @return
     * @throws StopTestException
     */
    public static String readCookieValue(String cookieName)
            throws StopTestException {
        Cookie cookie = null;

        WebDriver driver = PageFactory.getCurrentDriver();

        cookie = driver.manage().getCookieNamed(cookieName);
        if (cookie != null)
            return cookie.getValue();

        return null;
    }

    public static WebDriver getRemoteWebDriver(String browser)
            throws MalformedURLException, StopTestException {

        if(remoteWebDriver != null)
            return remoteWebDriver;

        String hubServerDetails = "";
        DesiredCapabilities capability = new DesiredCapabilities(browser, "",
                Platform.ANY);

        if (browser != null && browser.equalsIgnoreCase("firefox")) {
            FirefoxProfile profile = new FirefoxProfile();
            profile.setPreference("network.proxy.type", 5);
            profile.setPreference("network.http.phishy-userpass-length", 255);
            capability.setCapability(FirefoxDriver.PROFILE, profile);
            capability.setBrowserName("firefox");
        }

        // 11th dec 2015.  ChromeOptions commented out.
        // There is a bug (selenium 2.45, Chrome 47 and ChromeDriver 2.20) which
        // won't allow ChromeOptions to be passed from the Hub to Nodes.
        // https://github.com/SeleniumHQ/selenium/issues/369
        // Temp solution is to comment out the line of code below (ChromeOptions)
        // Permanent fix is to upgrade to (selenium 2.48 and ChromeDriver 2.20)
        // Pacaking request raised with DevTools.

        if (browser != null && browser.equalsIgnoreCase("chrome")) {
            capability.setBrowserName("chrome");
        }

        if (browser != null && browser.equalsIgnoreCase("ie")) {

            capability = DesiredCapabilities.internetExplorer();
            capability.setBrowserName("internet explorer");
            /*capability.setCapability(
                    InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,
                    true); */
            capability.setCapability(
                    CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION,
                    true);
            capability.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        }

        //hubServerDetails = Configuration.getConfiguration().getGridHubServerDetails();
        hubServerDetails="localhost:4444";

        URL hubUrl = new URL(
                "http://" + hubServerDetails + "/wd/hub");

        try {

            // Get the details of the selenium grid hub we want to use.
            // As of 18th June 2015 we have four possible selenium hubs.
            // 1. Local
            // 2. Regression (Used by the regression team)
            // 3. Projects (Used by BA.com project teams)
            // 4. Sauce Labs (Useed by regression and projects)

            // Is Sauce Labs being used? - set the proxy as a
            // desired cabability if running on a server.
            /*capability.setCapability("name",
                    Configuration.getConfiguration().getTestCaseName());

            for (Map.Entry<String, String> keyValue : PropertiesHelper
                    .getSeleniumProperties()
                    .entrySet()) {
                capability.setCapability(keyValue.getKey(),
                        keyValue.getValue());
            }*/

            remoteWebDriver = new RemoteWebDriver(hubUrl, capability);

            if (Configuration.getConfiguration().getDebugProperty()
                    .equalsIgnoreCase("true")) {

                Set<Cookie> cookies = remoteWebDriver.manage().getCookies();
                Iterator<Cookie> cookiesIter = cookies.iterator();
                while (cookiesIter.hasNext()) {
                    System.out.println(cookiesIter.next());
                }
            }

            printSessionId();

            remoteWebDriver.manage().deleteAllCookies();

        } catch (Exception e) {
            System.out.printf("Connecting to selenium grid: %s%n", hubUrl);
            System.out.printf("Desired capabilities: %s%n", capability);

            throw new StopTestException(
                    "Unable to create a RemoteWebDriver instance"
                            + e.getMessage(), e);
        }

        return remoteWebDriver;
    }

    private static void printSessionId()
            throws NoSuchMethodException, IllegalAccessException,
            InvocationTargetException {
        if (!(remoteWebDriver instanceof RemoteWebDriver))
            return;

        Method getSessionIdMethod = RemoteWebDriver.class
                .getMethod("getSessionId");
        String sessionId = getSessionIdMethod.invoke(remoteWebDriver)
                .toString();
        String message = String
                .format("SauceOnDemandSessionID=%1$s job-name=%2$s",
                        sessionId, System.getenv("SELENIUM_BUILD"));
        System.out.println(message);
    }


    /**
     * Create Chrome WebDriver instance after closing other driver instances
     *
     * @author n417179
     */
    public static WebDriver getChromeWebDriver() throws MalformedURLException,
            StopTestException {

        // Check to see if firefox is the current driver quit if not
        if (driver != null) {
            if (!(driver instanceof ChromeDriver)) {
                try {
                    driver.quit();
                } catch (Exception e) {
                    // Ignore

                }
            }
        }

        // See if firefox driver is running if not start a firefox driver
        try {
            driver.getTitle();
            return driver;
        } catch (Exception e) {

            System.setProperty("webdriver.chrome.driver", Configuration
                    .getConfiguration().getWebdriverForChrome());
//			DesiredCapabilities capability = DesiredCapabilities.chrome();
//			ChromeOptions options = new ChromeOptions();
//			options.addArguments("test-type");
//			capability.setCapability("chrome.binary", "<Path to binary>");
//			capability.setCapability(ChromeOptions.CAPABILITY, options);
            driver = new ChromeDriver();
            driver.manage().deleteAllCookies();
        }

        return driver;
    }

    public static WebDriver getSafariWebDriver() throws MalformedURLException,
            StopTestException {

        // Check to see if firefox is the current driver quit if not
        if (driver != null) {
            if (!(driver instanceof SafariDriver)) {
                try {
                    driver.quit();
                } catch (Exception e) {
                    // Ignore

                }
            }
        }

        try {
            driver.getTitle();
            return driver;
        } catch (Exception e) {

            System.setProperty("webdriver.safari.noinstall", "true");
            DesiredCapabilities capabilities = new DesiredCapabilities();
            capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
            String browserName =
                    null != Configuration.getConfiguration().getBrowser() ?
                            Configuration.getConfiguration().getBrowser()
                                    .toUpperCase() :
                            SAFARI_BROWSER;
            capabilities.setBrowserName(browserName);
            driver = new SafariDriver(capabilities);
            driver.manage().deleteAllCookies();
        }

        return driver;
    }

    public static WebDriver getIEWebDriver() throws MalformedURLException,
            StopTestException {

        // Check to see if firefox is the current driver quit if not
        if (driver != null) {
            if (!(driver instanceof InternetExplorerDriver)) {
                try {
                    driver.quit();
                } catch (Exception e) {
                    // Ignore

                }
            }
        }

        try {
            driver.getTitle();
            return driver;
        } catch (Exception e) {

            System.setProperty("webdriver.ie.driver", Configuration
                    .getConfiguration().getWebdriverForIE());
            DesiredCapabilities capabilities = new DesiredCapabilities();
            capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
            driver = new InternetExplorerDriver(capabilities);
            driver.manage().deleteAllCookies();
        }

        return driver;
    }

}